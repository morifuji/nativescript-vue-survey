# nativeScript-vue-survey

参考元gitは[こちら](https://github.com/nativescript-vue/vue-cli-template)

## 目的
- どのぐらいバグがあるか
- どこまで可用性があるか
- iOS/Android特有のプロパティやクセなどないか

## 前提条件

tnsコマンドが使えるか

```
tns docktor  // If it's ok, so you can set up an environment.
```

## 開発中

``` bash
yarn

// debug
yarn run debug:ios
// or
yarn run debug:android

//watch
yarn run watch:ios
// or
yarn run watch:android

// clear
yarn run clear
```

## ビルド時

[公式ページ](https://docs.nativescript.org/tooling/publishing/publishing-android-apps#overview)

### Android
まずは鍵を作る

```
$ keytool -genkey -v -keystore nativescript-vue-survey.jks -keyalg RSA -keysize 2048 -validity 10000 -alias nativescript-vue-survey
// 以下、パスワードとか名前とか聞かれる。
```

aliasが設定値と異なる場合があるので確認。aliasをメモる

```
$ keytool -list -keystore nativescript-vue-survey.jks
キーストアのパスワードを入力してください:  

キーストアのタイプ: JKS
キーストア・プロバイダ: SUN

キーストアには1エントリが含まれます

{ここにalias名が記載されている},2018/09/09, PrivateKeyEntry, 
証明書のフィンガプリント(SHA1): 41:06:25:2E:55:88:B7:0D:65:8B:72:E0:CA:63:0F:86:CF:71:17:50

```

yarnコマンドによるビルドがテンプレートでは準備されているが、使い勝手が悪すぎるので、手動で`tns build`する

```
$ cd dist
$ tns build android \
--release \
--key-store-path {さっきのkjsファイルパス} \
--key-store-password {設定したパスワード} \
--key-store-alias {さっき確認したエイリアス} \
--key-store-alias-password {設定したパスワード。--key-store-passwordと同じ}
```

`/dist/platforms/android/app/build/outputs/apk/release/app-release.apk`に出力される


### iOS
**実機でテストする場合は、 `--for-device`を必ずつけること！！！！！！👀**

```
$ cd dist
$ tns build ios --release --for-device
```

