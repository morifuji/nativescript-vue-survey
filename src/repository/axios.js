import originalAxios from "axios";

const axios = originalAxios.create({
  headers: { "Content-Type": "application/json" }
});

export default {
  post(path, params = {}) {
    return axios.post(path, params);
  },
  put(path, params = {}) {
    return axios.put(path, params);
  },
  patch(path, params = {}) {
    return axios.patch(path, params);
  },
  delete(path, params = {}) {
    return axios.delete(path, params);
  },
  get(path, params = {}) {
    return axios.get(path, { params: params });
  },

  /**
   * サーバーとのエラーをフォーマット
   * @param {*} error
   */
  formatError(error) {
    if (error.response) {
      const status = error.response.status;
      const data = error.response.data;
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // 400~499までの間ならエラーメッセージが返却されるはず
      if (400 <= status < 500 || Array.isArray(data)) {
        // 配列として返却
        return data.map(errorObject => errorObject.message);
      }
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      return "データを取得できませんでした。時間を開けて試してください。";
    }

    return "不明なエラーが発生しました";
  }
};
