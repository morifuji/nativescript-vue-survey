import Vue from 'nativescript-vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../components/Home';
import Calendar from '../components/Calendar';
import Counter from '../components/Counter';
import Gauge from '../components/Gauge';
import Login from '../components/Login';
import EyeCandy from '../components/EyeCandy/Index';
import Gesture from '../components/Gesture';
import Camera from '../components/Camera';
import Image from '../components/Image';
import Sqlite from '../components/Sqlite';
import GitHubTrendApi from '../components/GitHubTrendApi';
import WebView from '../components/WebView';

const router = new VueRouter({
  pageRouting: true,
  routes: [
    {
      path: '/home',
      component: Home,
      meta: {
        title: 'Home',
      },
    },
    {
      path: '/calendar',
      component: Calendar,
      meta: {
        title: 'カレンダー',
      },
    },
    {
      path: '/image',
      component: Image,
      meta: {
        title: 'イメージサンプル',
      },
    },
    {
      path: '/sqlite',
      component: Sqlite,
      meta: {
        title: 'sqlite',
      },
    },
    {
      path: '/eye_candy',
      component: EyeCandy,
      meta: {
        title: '愛キャンディー',
      },
    },
    {
      path: '/web_view',
      component: WebView,
      meta: {
        title: 'webビュー',
      },
    },
    {
      path: '/camera',
      component: Camera,
      meta: {
        title: 'カメラ',
      },
    },
    {
      path: '/git_hub_trend_api',
      component: GitHubTrendApi,
      meta: {
        title: 'GitHubトレンド！',
      },
    },
    {
      path: '/gesture',
      component: Gesture,
      meta: {
        title: 'gesture',
      },
    },
    {
      path: '/login',
      component: Login,
      meta: {
        title: 'ログイン',
      },
    },
    {
      path: '/gauge',
      component: Gauge,
      meta: {
        title: 'がうぐ',
      },
    },
    {
      path: '/counter',
      component: Counter,
      meta: {
        title: 'Counter',
      },
    },
    {path: '*', redirect: '/home'},
  ],
});

router.replace('/home');

module.exports = router;
