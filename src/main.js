// require('nativescript-ui-calendar/vue');
// require('nativescript-ui-gauge/vue');

import Vue from 'nativescript-vue';

import router from './router';

import store from './store';

import './styles.scss';

import axios from "./repository/axios";


// Uncomment the following to see NativeScript-Vue output logs
Vue.config.silent = false;

Vue.registerElement('RadCalendar', ()=> require('nativescript-ui-calendar').RadCalendar)

Vue.registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView)

// Vue.registerElement('RadRadialGauge', ()=> require('nativescript-ui-gauge').RadRadialGauge)
// Vue.registerElement('RadialScale', ()=> require('nativescript-ui-gauge').RadialScale)
// Vue.registerElement('ScaleStyle', ()=> require('nativescript-ui-gauge').ScaleStyle)
// Vue.registerElement('RadialBarIndicator', ()=> require('nativescript-ui-gauge').RadialBarIndicator)
// Vue.registerElement('BarIndicatorStyle', ()=> require('nativescript-ui-gauge').BarIndicatorStyle)
// Vue.registerElement('RadialNeedle', ()=> require('nativescript-ui-gauge').RadialNeedle)

Vue.prototype.$axios = axios;

new Vue({
  router,

  store,

}).$start();
